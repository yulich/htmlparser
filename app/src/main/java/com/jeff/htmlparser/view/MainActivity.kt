package com.jeff.htmlparser.view

import com.jeff.htmlparser.R
import com.jeff.htmlparser.view.base.BaseActivity
import com.log.JFLog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.coroutines.CoroutineContext

class MainActivity : BaseActivity(), CoroutineScope {

    private val viewModel by viewModel<MainViewModel>()

    private var activityJob: Job? = null

    override val coroutineContext: CoroutineContext
        get() {
            if (activityJob == null || activityJob!!.isCancelled) {
                activityJob = Job()
            }

            return Dispatchers.Main + activityJob!!
        }

    override fun getLayoutId() = R.layout.activity_main

    override fun onResume() {
        super.onResume()

        JFLog.i("onResume")

        launch {
            val sb = StringBuilder()

            viewModel.wikiParser1()
                .flowOn(Dispatchers.IO)
                .catch {
                    JFLog.e(it)
                }
                .onStart {
                    JFLog.d("Parser1 start")
                }
                .onCompletion {
                    JFLog.d("Parser1 completion")

                    tv_1.text = sb.toString()
                }
                .collect {
                    JFLog.d(it)
                    sb.append("$it\n\n")
                }
        }
    }

    override fun onPause() {
        super.onPause()

        JFLog.i("onPause")

        activityJob?.cancel()
    }
}
