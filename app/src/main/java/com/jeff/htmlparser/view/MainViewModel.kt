package com.jeff.htmlparser.view

import com.jeff.htmlparser.view.base.BaseViewModel
import kotlinx.coroutines.flow.flow
import org.jsoup.Jsoup

class MainViewModel : BaseViewModel() {

    fun wikiParser1() = flow {
        val doc = Jsoup.connect("http://en.wikipedia.org/").get()

        emit("Doc title: ${doc.title()}")

        val newsHeadlines = doc.select("#mp-itn b a")
        for (headline in newsHeadlines) {
            "Head title: ${headline.attr("title")}, href: ${headline.absUrl("href")}".also {
                emit(it)
            }
        }
    }
}